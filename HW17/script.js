// Завдання
// Створити об'єкт "студент" та проаналізувати його табель.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу
// jQuery або React.

// Технічні вимоги:
// Створити порожній об'єкт student, з полями name та lastName.
// Запитати у користувача ім'я та прізвище студента, отримані значення записати
// у відповідні поля об'єкта.
// У циклі запитувати у користувача назву предмета та оцінку щодо нього.
// Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл.
// Записати оцінки з усіх предметів як студента tabel.
// порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає,
// вивести повідомлення "Студент переведено на наступний курс".
// Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення
// Студенту призначено стипендію.

const student = {
  lastName: "",
  name: "",
};

let studentName = "";

while (studentName.length < 2) {
  studentName = prompt("Ім'я та прізвище студента?").trim().split(" ");
}

student["name"] = studentName[0].trim();
student["lastName"] = studentName[1].trim();

student.table = {};

while (true) {
  let subject = prompt("Предмет?");
  if (!subject) {
    break;
  }
  let marks = +prompt("Оцінка з цього предмету:");
  student.table[subject] = marks;
}

if (!Object.values(student.table).some((i) => i < 4)) {
  alert("Студент переведено на наступний курс");
}

const sumMarks = Object.values(student.table).reduce((acc, next) => {
  acc += next;
  return acc;
}, 0);

if (sumMarks / Object.values(student.table).length > 7) {
  alert("Студенту призначено стипендію");
}
